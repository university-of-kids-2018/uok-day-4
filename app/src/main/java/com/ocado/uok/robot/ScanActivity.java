package com.ocado.uok.robot;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;

import com.ocado.uok.robot.basket.Product;
import com.ocado.uok.robot.basket.ShoppingTask;
import com.ocado.uok.robot.result.ShoppingTaskResults;
import com.ocado.uok.robot.time.TimeRecord;
import com.ocado.uok.skilo.scanner.ScannerResult;

import static com.ocado.uok.robot.R.layout;

/**
 * Ekran ze skanowaniem produktów.
 */
public class ScanActivity extends ActivityWithSomeMethods {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(layout.activity_scan);

        ScannerResult scannerResult =
                ViewModelProviders.of(this).get(ScannerResult.class);
        ShoppingTask shoppingTask =
                ViewModelProviders.of(this).get(ShoppingTask.class);
        ShoppingTaskResults shoppingTaskResults =
                ViewModelProviders.of(this).get(ShoppingTaskResults.class);

        shoppingTask.setProductsToBuy(Product.buildRandomList());
        getTimer().start();

        scannerResult.getLastScanned().observe(this, scannedText -> {
            int ileDoKupienia = 0;
            Product product = Product.of(scannedText);
            for (Product p : shoppingTask.getProductsToBuy()) {
                if (p == product) {
                    ileDoKupienia++;
                }
            }

            int ileWKoszyku = 0;
            for (Product p : shoppingTask.getProductsInBasket()) {
                if (p == product) {
                    ileWKoszyku++;
                }
            }
            if (ileWKoszyku < ileDoKupienia) {
                shoppingTask.addToBasket(product);
                if (shoppingTask.isComplete()) {
                    TimeRecord timeRecord = getTimer().end();
                    shoppingTaskResults.save(timeRecord);
                    showToast("Zamówienie skompletowane w " + timeRecord.getReadableDuration());
                    finish();
                } else {
                    showToast("Dodano do koszyka " + scannedText);
                }
            } else {
                showToast(scannedText + " nie jest do zebrania");
            }

            doWithDelay(1000, () -> scannerResult.getResumeCamera().call());
        });
    }

}
