package com.ocado.uok.robot.time;

import java.util.Date;

/**
 * Pomiar czasu wykonywania zadania
 */
public class Timer {

    private Long startTime;
    private Long endTime;

    public void start() {
        this.startTime = now();
    }

    public TimeRecord end() {
        endTime = now();
        return new TimeRecord(startTime, endTime);
    }

    public TimeRecord getTimeRecord() {
        if(startTime == null || endTime == null) {
            throw new IllegalStateException();
        }
        return new TimeRecord(startTime, endTime);
    }

    private long now() {
        return new Date().getTime();
    }
}

