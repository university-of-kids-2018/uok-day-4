package com.ocado.uok.robot;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TableLayout;

import com.ocado.uok.robot.result.ResultTableRow;
import com.ocado.uok.robot.result.ShoppingTaskResults;
import com.ocado.uok.robot.result.TimeRecordAdapter;
import com.ocado.uok.robot.time.TimeRecord;

import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        ShoppingTaskResults shoppingTaskResults =
                ViewModelProviders.of(this).get(ShoppingTaskResults.class);

        TableLayout tableLayout = findViewById(R.id.resultsTable);
        shoppingTaskResults.getTimeRecords().observe(this, timeRecords -> {
            for(TimeRecord timeRecord: timeRecords) {
                tableLayout.addView(ResultTableRow.build(0, timeRecord, this));
            }
        });
    }

    private void presentRecords(List<TimeRecord> records, int resultListViewId,
                                         int itemLayoutId, int durationTextId, int startTimeTextId, int positionTextId) {
        RecyclerView recyclerView = findViewById(resultListViewId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new TimeRecordAdapter(records, itemLayoutId, durationTextId, startTimeTextId, positionTextId));
    }
}
