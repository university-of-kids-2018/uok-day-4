package com.ocado.uok.robot.time;

import android.support.annotation.NonNull;

public class TimeRecord implements Comparable<TimeRecord> {

    private final long startTimestamp;
    private final long endTimestamp;

    public TimeRecord(long startTime, long endTime) {
        this.startTimestamp = startTime;
        this.endTimestamp = endTime;
    }

    public Long getStartTimestamp() {
        return startTimestamp;
    }

    public Long getEndTimestamp() {
        return endTimestamp;
    }

    public Long getDuration() {
        return endTimestamp - startTimestamp;
    }

    public String getReadableDuration() {
        long totalSeconds = getDuration() / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        long millis = getDuration() % 1000;
        return String.format("%02d:%02d.%03d", minutes, seconds, millis);
    }

    @Override
    public int compareTo(@NonNull TimeRecord timeRecord) {
        return getDuration().compareTo(timeRecord.getDuration());
    }
}
