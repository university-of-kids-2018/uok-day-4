package com.ocado.uok.robot;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ocado.uok.robot.audio.SfxPlayer;
import com.ocado.uok.robot.time.Timer;
import com.ocado.uok.skilo.scanner.ScannerResult;

public class ActivityWithSomeMethods extends AppCompatActivity {

    private Timer timer = new Timer();

    private SfxPlayer sfxPlayer = new SfxPlayer();

    public Timer getTimer() {
        return timer;
    }

    public SfxPlayer getAudioPlayer() {
        return sfxPlayer;
    }

    public void doWithDelay(long time, Runnable action) {
        // czekaj zadany czas przed wykonaniem zadania
        new Handler().postDelayed(action, time);
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    AlertDialog displayDialog(String title) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(" ")
                .create();
        hideDialogAfter(alertDialog, 3000);
        return alertDialog;

    }

    AlertDialog displayDialog(String title, int iconId) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(" ")
                .setIcon(iconId)
                .create();
        hideDialogAfter(alertDialog, 3000);
        return alertDialog;
    }

    public void displayDialogWithAction(String title, String info, Runnable action) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(info)
                .setNeutralButton("OK", (d, w) -> action.run())
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sfxPlayer.destroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sfxPlayer.init(this);
    }

    public void hideDialogAfter(AlertDialog alertDialog, long milliseconds) {
        final AppCompatActivity currentActivity = this;
        alertDialog.show();
        new CountDownTimer(milliseconds, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                ViewModelProviders.of(currentActivity).get(ScannerResult.class).getResumeCamera().call();
                alertDialog.dismiss();
            }
        }.start();
    }

}
