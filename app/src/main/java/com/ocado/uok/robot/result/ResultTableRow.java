package com.ocado.uok.robot.result;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.widget.TableRow;
import android.widget.TextView;

import com.ocado.uok.robot.time.TimeRecord;

import java.text.SimpleDateFormat;

import static android.view.Gravity.RIGHT;

public class ResultTableRow {
    public static TableRow build(int position, TimeRecord timeRecord, Activity activity) {
        int width = getWidth(activity);

        TableRow l = createScoreRow(activity);
        l.addView(createCell(position + ".", width / 8, activity));
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
        l.addView(createCell(dateFormat.format(timeRecord.getStartTimestamp()), width / 3, activity));
        l.addView(createCell(timeRecord.getReadableDuration(), width / 4, activity));
        return l;
    }

    private static int getWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    private static TextView createCell(String value, int width, Activity activity) {
        TextView tv = new TextView(activity);
        tv.setMinWidth(width);
        tv.setGravity(RIGHT);
        tv.setText(value);
        return tv;
    }

    private static TableRow createScoreRow(Activity activity) {
        return new TableRow(activity);
    }
}
